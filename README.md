
# Requirements

## Security

It must be ensured that no vote is lost. 

## Secrecy

It must be ensured that the voters choice is anonymous, i. e. it cannot be deduced from the published data how they voted each. The rationale behind this is to prevent attempts to influence voters by intimidation, blackmailing, and vote buying. 

## Auditability

In the case that someone is questioning the regularity of the election, an auditor must be able to check validate the process.    



# Proposed process

## 1. Setting up the system

To start an election, the `ElectionAdministrator`

1. determines how the election should be called (`election.name`)
3. determines the choices the voters can select from (`election.choices`),
4. determines the closing date of the election (`election.closingDate`)
1. creates a list of email addresses of all voters `(election.emailAddressesOfVoters)`, 
0. determines if voters should be able to check that their vote has been counted in the result (`election.isTracableByVoter`),  
1. determines if the list of voters is public  (`election.isVoterRegistryVisibleToVoters`, `election.isVoterRegistryVisibleToTheWorld`),
1. determines if the information is public if a voter has voted or not (`election.isVoterStatusVisibleToVoters`, `election.isVoterStatusVisibleToTheWorld`),
2. assigns the role `ParcelOpener` to someone and asks this person for a public encryption key  (`parcelOpener.publicKey`), 
2. assigns the role `LetterOpener` to someone and asks this person for a public encryption key (`letterOpener.publicKey`), 

## 2. Inviting voters to vote 

An email is sent to each email address in `election.emailAddressesOfVoters`:

> Hello, voter,  
> for taking part in the election `election.name`, please visit this non-secret `election.url`.  The closing date is `election.closingDate`.
     

## 3. Voting of voter_i

### Voter authenticates passwordlessly

The voter `voter_i` visits  `election.url`. 

For authentication, `voter_i`  has to provide his email address `voter_i.emailAddress`. If the given email address is in the voters' email adresses list (election.emailAddressesOfVoters), the system sends and authentication token to this email address. 

> Hello again, voter,  
> to log into the voting system, please open this secret link `election.url?token=<generatedToken>`.  
> This link is valid for three minutes only. This short duration is a security measure in case this message is transmitted via an insecure channel on its ways from me to you. But no worries, we can repeat this procedure until you are quick enough. 


### `Voter` votes

The voter's webbrowser reads the voter's vote and packs it with the voter's private vote marker into the vote letter:
 
 ```
vote_letter = {content: vote, marker: voter_i._voteMarker}
closed_vote_letter = letterEncryptor.encrypt(vote_letter)
```

The `closed_vote_letter` is combined with the voter's private letter marker to the `vote_parcel` and closed:

```
vote_parcel = {content: closed_vote_letter, marker: voter_i._parcelMarker}
closed_vote_parcel = parcelEncryptor.encrypt(vote_parcel)
```

Finally, the `closed_vote_parcel` is published to the `parcelReceptionist`

### ParcelReceptionist publishes parcel message

The `ParcelReceptionist` checks the authorization of each incoming parcels. If a parcel is authorized he adds it to the `MessageStore` (or replaces an previously accepted parcel by that voter). 


### Checking parcel receipt

The voter can subscribe to the MessageStore and check if his parcel has been added to the collection of parcels. 

## 4. Opening parcels and publishing letters

After the election's closing date, the `ParcelOpener` 

1. reads the list of vote parcel messages from the `MessageStore`,
2. decrypts the content of each parcel message, i. e. the parcel, with his private key (`ParcelOpener.privateKey`), 
3. and publishes a list of letter messages (sorted by the letter stamps) back to the `MessageStore`.

## 5. Opening letters and publishing votes

The `LetterOpener` 
1. reads the list of vote letter messages from the `MessageStore`, 
2. decrypts the content of each letter message with his private key (`LetterOpener.privateKey`),
3. and publishes a list of (vote, vote stamp) pairs (sorted by the encrypted vote stamps `Letter.stamp`) back to the `MessageStore`.

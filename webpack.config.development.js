"use strict";

const merge = require('webpack-merge');
const common = require('./webpack.config.common.js');
const path = require("path");
const packageJSON = require("./package.json")

const BUILD_PATH = (process.env.BUILD_PATH)
    ? path.join(__dirname, process.env.BUILD_PATH)
    : path.join(__dirname, "_build", packageJSON.version)

module.exports = merge(common, {
    // mode: "development",
    devtool: 'inline-source-map',
    watch: true,
    // devServer: {
    //     contentBase: BUILD_PATH,
    //     compress: true,
    //     port: 9000
    // }

});
 

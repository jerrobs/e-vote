"use strict";

const merge = require('webpack-merge');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const common = require('./webpack.config.common.js');
const path = require("path");
const packageJSON = require("./package.json")

const BUILD_PATH = (process.env.BUILD_PATH)
    ? path.join(__dirname, process.env.BUILD_PATH)
    : path.join(__dirname, "_build", packageJSON.version)

module.exports = merge(common, {
    // mode: "production",
    plugins: [


        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /de|en/),

        new UglifyJSPlugin({
            parallel: 4,
            cache: true,
            uglifyOptions: {
                sourceMap: false,
                compress: true,
                ie8: false,
                ecma: 8,
                // parse: {...options},
                // mangle: {
                //     // ...options,
                //     props: {
                //         // mangle property options
                //     }
                // },
                output: {
                    comments: false,
                    beautify: false,
                    // ...options
                },
                warnings: true
            }
        }),

    ]
});
 

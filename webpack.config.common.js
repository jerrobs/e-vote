"use strict";
const path = require("path");
// const HtmlWebpackPlugin = require('htmlk-webpack-plugin');
const packageJSON = require("./package.json")

const BUILD_PATH = (process.env.BUILD_PATH)
    ? path.join(__dirname, process.env.BUILD_PATH)
    : path.join(__dirname, "_build", packageJSON.version)

module.exports = {

    entry: {

        "evote": [
            "./_source/lib/rsa-test.js"
        ]
    },
    output: {

        library: "evote",
        path: path.join(BUILD_PATH, "scripts"),
        filename: "[name]--webpacked.js",

    },
    target: "web",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                }
            },

        ]
    },

    stats: {
        colors: true,
        modules: true,
        reasons: true
    },

    // plugins: [
    //     new HtmlWebpackPlugin({
    //         hash: true,
    //         filename: path.join(BUILD_PATH, "index.html")
    //     })
    // ]

};


#!/usr/bin/env sh

#
BUILD_DIR=./_build/0.0.1

pandoc  -o $BUILD_DIR/paper.tex -s README.md

pandoc  --variable papersize=a5paper --variable fontsize=10spt -o $BUILD_DIR/paper.pdf -s README.md --template=_scripts/tufte-handout.tex --pdf-engine=pdflatex
#pandoc --filter pandoc-citeproc --csl=$BIBLIOGRAPHY_DIR\apa.csl --bibliography=$BIBLIOGRAPHY_DIR\bibliopgraphy.bib --variable papersize=a4paper  -o $BUILD_DIR\bibliography.pdf -s $BIBLIOGRAPHY_DIR\bibliography.md --template=$BIBLIOGRAPHY_DIR\eisvogel.tex --pdf-engine=pdflatex
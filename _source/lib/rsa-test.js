"use strict";

const assert = require("assert")
const NodeRSA = require('node-rsa');
const con = require('node-rsa');
const nodeNSAOptions = {encryptionScheme: 'pkcs1'}

const uuid = require('uuid/v1');


window.realConsoleLog = console
console.log = (p) => {
    const element = document.querySelector(".console")

        element.innerHTML += '<pre>' + p + '</pre>';

}


//
class Voter {

    constructor(params) {

        this.emailAddress = params.emailAddress
        this._encryptor = params.encryptor
        this.parcelReceptionist = params.parcelReceptionist
        this.parcelEncryptor = new NodeRSA(params.parcelPublicKey, "public", nodeNSAOptions)
        this.letterEncryptor = new NodeRSA(params.letterPublicKey, "public", nodeNSAOptions)
        this.collectionStore = params.collectionStore

        this._voteMarkerEncryptor = new NodeRSA({b: 512}, nodeNSAOptions);
        this._letterMarkerEncryptor = new NodeRSA({b: 512}, nodeNSAOptions);
        this._parcelMarkerEncryptor = new NodeRSA({b: 512}, nodeNSAOptions);
        this._marker = "<secret marker of " + this.emailAddress + ">"

    }


    makeVote(vote) {

        console.log(`[voter: ${this.emailAddress}] I'am making the vote ${vote}`)

        console.log(`[voter: ${this.emailAddress}] creating vote letter`)
        const letter = {
            content: vote,
            marker: this._voteMarkerEncryptor.encrypt({secret: this._marker}, 'base64')
        }


        console.log(`[voter: ${this.emailAddress}] closing (= encrypting) vote letter`)

        const encryptedLetter = this.letterEncryptor.encrypt(letter, 'base64')

        console.log(`[voter: ${this.emailAddress}] creating vote parcel`)

        const parcel = {
            content: encryptedLetter,
            marker: this._letterMarkerEncryptor.encrypt({secret: this._marker}, 'base64')
        }

        const encryptedParcel = this.parcelEncryptor.encrypt(parcel, 'base64')

        console.log(`[voter: ${this.emailAddress}] closing (= encrypting) vote letter`)

        const parcelMessage = {
            content: encryptedParcel,
            marker: this._parcelMarkerEncryptor.encrypt({secret: this._marker}, 'base64')
        }


        const encryptedParcelMessage = this._encryptor.encryptPrivate(parcelMessage, 'base64')

        parcelReceptionist.pushParcelMessage(this.emailAddress, encryptedParcelMessage)

    }

    checkVotes() {

        const votes = collectionStore.getCollections().votes;

        const decrpytedMarkers = votes.map(entry => {

                try {
                    return this._voteMarkerEncryptor.decrypt(entry.marker, "json").secret
                } catch (e) {
                    // console.log("error")

                }
            }
        )
        if (decrpytedMarkers.indexOf(this._marker) > -1)
        {
            console.log("[" + this.emailAddress + "] My vote has been counted.")
            console.log("[" + this.emailAddress + " to himself] My vote '" + votes[decrpytedMarkers.indexOf(this._marker)].content + "' has been counted indeed. It's in the vote list on position " + decrpytedMarkers.indexOf(this._marker))} else {
            console.log("[" + this.emailAddress + "] My vote hasn't been counted.")
        }
    }
}

class ParcelReceptionist {

    constructor(collectionStore) {

        this._collectionStore = collectionStore;
        this._parcelMessages = new Map()
        this._voterDictionary = new Map();
    }


    registerVoter(emailAddress, publicKey) {

        console.log(`[ParcelReceptionist] registering voter (emailAddress: ${emailAddress}, publicKey: ${publicKey})`)
        this._voterDictionary.set(emailAddress, publicKey)
    }

    pushParcelMessage(emailAddress, encryptedParcelMessage) {

        // if authorized

        assert.ok(this._voterDictionary.has(emailAddress), "not registerted")


        const encryptor = new NodeRSA(this._voterDictionary.get(emailAddress), nodeNSAOptions);


        const parcelMessage = encryptor.decryptPublic(encryptedParcelMessage, "json")

        this._parcelMessages.set(emailAddress, parcelMessage)

        this._collectionStore.setParcels(Array.from(this._parcelMessages.values()))

    }

}

class CollectionOpener {

    constructor(inputCollectioName, outputCollectioName, collectionStore) {

        this._inputCollectioName = inputCollectioName;
        this._outputCollectioName = outputCollectioName;
        this._collectionStore = collectionStore;

        this._encryptor = new NodeRSA({b: 512}, nodeNSAOptions);
    }

    getPublicKey() {
        return this._encryptor.exportKey(("public"))
    }

    decryptCollection() {

        const collection = this._collectionStore.getCollections()[this._inputCollectioName]


        const decryptedCollection =
            collection.map(item => {
                return this._encryptor.decrypt(item.content, "json")
            }).sort(
                (item1, item2) =>
                    item1.marker.localeCompare(item2.marker)
            )

        this._collectionStore.setCollection(this._outputCollectioName, decryptedCollection)
    }

}

class CollectionStore {

    constructor() {

        this._collections = {
            parcels: [],
            letters: [],
            votes: []
        }
    }

    setParcels(parcels) {
        // if authorized, i.e. evocation by parcelReceptionist
        this._collections.parcels = parcels
    }

    setLetters(letters) {
        // if authorized, i.e. evocation by parcelOpener
        this._collections.letters = letters
    }

    setVotes(votes) {
        // if authorized, i.e. evocation by letterOpener
        this._collections.votes = votes
    }

    setCollection(collectionName, collection) {
        // if authorized, i.e. evocation by letterOpener

        this._collections[collectionName] = collection;
    }

    getCollections() {
        return this._collections
    }

}

const collectionStore = new CollectionStore()
const parcelReceptionist = new ParcelReceptionist(collectionStore)
const parcelOpener = new CollectionOpener("parcels", "letters", collectionStore)
const letterOpener = new CollectionOpener("letters", "votes", collectionStore)


const voters = [...Array(5).keys()].map(i => {

    console.log(`[system] creating voter: ${i}`)

    const voterParams = {
        emailAddress: `voter${i}@dirk-rathje.de`,
        encryptor: new NodeRSA({b: 512}, nodeNSAOptions),
        parcelReceptionist: parcelReceptionist,
        parcelPublicKey: parcelOpener.getPublicKey(),
        letterPublicKey: letterOpener.getPublicKey(),
        collectionStore: collectionStore

    }

    parcelReceptionist.registerVoter(voterParams.emailAddress, voterParams.encryptor.exportKey("public"));

    return new Voter(voterParams)
})


voters[0].makeVote("A")
voters[1].makeVote("B")
voters[2].makeVote("C")
voters[3].makeVote("D")
voters[4].makeVote("E")



console.log(`[messageStore]: ` + JSON.stringify(collectionStore.getCollections(), null, "   "))

console.log(`[parcelOpener]: reading parcels, opeining them and publishing result`)

parcelOpener.decryptCollection()
console.log(`[messageStore]: ` + JSON.stringify(collectionStore.getCollections(), null, "   "))

console.log(`[letterOpener]: reading letters, opening (= decrypting) them and publishing result`)

//
letterOpener.decryptCollection()
//


console.log(`[messageStore]: ` + JSON.stringify(collectionStore.getCollections(), null, "   "))

voters[0].checkVotes()
voters[1].checkVotes()
voters[2].checkVotes()
voters[3].checkVotes()
voters[4].checkVotes()